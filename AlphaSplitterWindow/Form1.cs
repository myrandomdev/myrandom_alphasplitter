﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlphaSplitterWindow
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar1.Value = 0;

            textBox1.Text = Properties.Settings.Default.lastIn;
            textBox2.Text = Properties.Settings.Default.lastOut;

            if (System.IO.File.Exists(Properties.Settings.Default.app_path) == false)
            {
                if (openFileDialog2.ShowDialog() == DialogResult.OK)
                {
                    Properties.Settings.Default.app_path = openFileDialog2.FileName;
                    Properties.Settings.Default.Save();
                }
                else
                {
                    return;
                }
            }
        }

        #region MAIN TAB

        string inputDirectory = "";
        string outputDirectory = "";

        private void button1_Click(object sender, EventArgs e)
        {
            inputDirectory = textBox1.Text;
            outputDirectory = textBox2.Text;            

            progressBar1.Value = 0;
            button1.Enabled = false;

            List<string> filePaths = new List<string>(System.IO.Directory.EnumerateFiles(inputDirectory, "*.png"));

            backgroundWorker1.RunWorkerAsync(filePaths);
        }

        //void splitAlphaForFile(string inputFilePath, string outputFilePath)
        //{
        //    using (System.Drawing.Bitmap srcBitmap = new System.Drawing.Bitmap(inputFilePath)) 
        //    {
        //        using (System.Drawing.Bitmap dstBitmap = new System.Drawing.Bitmap(srcBitmap.Width * 2, srcBitmap.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
        //        {
        //            #region LOCK SRC

        //            // Lock the bitmap's bits.  
        //            Rectangle rectSrc = new Rectangle(0, 0, srcBitmap.Width, srcBitmap.Height);
        //            System.Drawing.Imaging.BitmapData bmpDataSrc =
        //                srcBitmap.LockBits(rectSrc, System.Drawing.Imaging.ImageLockMode.ReadWrite,
        //                srcBitmap.PixelFormat);

        //            // Get the address of the first line.
        //            IntPtr ptrSrc = bmpDataSrc.Scan0;

        //            // Declare an array to hold the bytes of the bitmap.
        //            int bytesSrc = Math.Abs(bmpDataSrc.Stride) * srcBitmap.Height;
        //            byte[] rgbValuesSrc = new byte[bytesSrc];

        //            int bytesPerPixelSrc = Bitmap.GetPixelFormatSize(srcBitmap.PixelFormat) / 8;
        //            int widthInBytesSrc = bmpDataSrc.Width * bytesPerPixelSrc;

        //            // Copy the RGB values into the array.
        //            System.Runtime.InteropServices.Marshal.Copy(ptrSrc, rgbValuesSrc, 0, bytesSrc);

        //            #endregion

        //            #region LOCK DST
        //            // Lock the bitmap's bits.  
        //            Rectangle rectDst = new Rectangle(0, 0, dstBitmap.Width, dstBitmap.Height);
        //            System.Drawing.Imaging.BitmapData bmpDataDst =
        //                dstBitmap.LockBits(rectDst, System.Drawing.Imaging.ImageLockMode.ReadWrite,
        //                dstBitmap.PixelFormat);

        //            // Get the address of the first line.
        //            IntPtr ptrDst = bmpDataDst.Scan0;

        //            // Declare an array to hold the bytes of the bitmap.
        //            int bytesDst = Math.Abs(bmpDataDst.Stride) * dstBitmap.Height;
        //            byte[] rgbValuesDst = new byte[bytesDst];

        //            int bytesPerPixelDst = Bitmap.GetPixelFormatSize(dstBitmap.PixelFormat) / 8;
        //            int widthInBytesDst = bmpDataDst.Width * bytesPerPixelDst;

        //            // Copy the RGB values into the array.
        //            System.Runtime.InteropServices.Marshal.Copy(ptrDst, rgbValuesDst, 0, bytesDst);

        //            #endregion

        //            for (int y = 0; y < bmpDataSrc.Height; y++)
        //            {
        //                int currentLineSrc = y * bmpDataSrc.Stride;
        //                int currentLineDst = y * bmpDataDst.Stride;

        //                for (int x = 0; x < bmpDataSrc.Width; x++)
        //                {
        //                    byte B = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 0];
        //                    byte G = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 1];
        //                    byte R = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 2];
        //                    byte A = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 3];

        //                    double dB = B;
        //                    double dG = G;
        //                    double dR = R;
        //                    double dA = A;

        //                    double dAMax = 255;

        //                    // STRAIT ALPHA
        //                    if (radioButton1.Checked)
        //                    {
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = B;
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = G;
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = R;
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
        //                    }
        //                    // BLAK MATTE
        //                    else if (radioButton2.Checked)
        //                    {
        //                        // Cb = 0
        //                        // ( Cu ∙ a ) 

        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = (byte) (dB * dA / dAMax);
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = (byte) (dG * dA / dAMax);
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = (byte) (dR * dA / dAMax);
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
        //                    }
        //                    // WHITE MATTE
        //                    else if (radioButton2.Checked)
        //                    {
        //                        // Cb = 255
        //                        // Cm = ( Cu ∙ a ) + [ Cb ∙ ( amax - a ) ]

        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = clamp_0_255((byte) ((dB * dA / dAMax) + (dAMax - dA)));
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = clamp_0_255((byte) ((dG * dA / dAMax) + (dAMax - dA)));
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = clamp_0_255((byte) ((dR * dA / dAMax) + (dAMax - dA)));
        //                        rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
        //                        rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
        //                    }
        //                }
        //            }

        //            // Unlock the bits.
        //            srcBitmap.UnlockBits(bmpDataSrc);

        //            // Copy the RGB values back to the bitmap
        //            System.Runtime.InteropServices.Marshal.Copy(rgbValuesDst, 0, ptrDst, bytesDst);

        //            // Unlock the bits.
        //            dstBitmap.UnlockBits(bmpDataDst);

        //            dstBitmap.Save(outputFilePath);
        //        }
        //    }
        //}

        //byte clamp_0_255(int b)
        //{
        //    if (b > 255)
        //        b = 255;
        //    if (b < 0)
        //        b = 0;

        //    return (byte)b;
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;

                Properties.Settings.Default.lastIn = textBox1.Text;
                Properties.Settings.Default.Save();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog2.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog2.SelectedPath;
                
                Properties.Settings.Default.lastOut = textBox2.Text;
                Properties.Settings.Default.Save();
            }
        }


        Base.Apha.AlphaSplitType AlphaSplitType
        {
            get
            {
                if (radioButton2.Checked)
                    return Base.Apha.AlphaSplitType.Black;
                else if (radioButton3.Checked)
                    return Base.Apha.AlphaSplitType.White;
                else
                    return Base.Apha.AlphaSplitType.None;
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> filePaths = e.Argument as List<string>;
            for (int i = 0; i < filePaths.Count; ++i)
            {
                string filePath = filePaths[i];

                string fileName = System.IO.Path.GetFileName(filePath);                
                Base.Apha.splitFileColorAndAlpha(
                    System.IO.Path.Combine(inputDirectory, fileName), 
                    System.IO.Path.Combine(outputDirectory, normalizeFileNameNumber(fileName, 4)),
                    AlphaSplitType);

                float f = i;
                float t = filePaths.Count;

                backgroundWorker1.ReportProgress((int)((f / t) * 100));
            }
        }

        string normalizeFileNameNumber(string fileName, int digits)
        {
            int startIndex = -1;
            int count = 0;
            string s = "";
            for (int i = 0; i < fileName.Length; ++i)
            {
                if (char.IsDigit(fileName[i]))
                {
                    s = s + fileName[i];

                    if (startIndex == -1)
                    {
                        startIndex = i;
                    }

                    count++;
                }
            }

            int j = 0;
            if (int.TryParse(s, out j))
            {
                string ext = System.IO.Path.GetExtension(fileName);

                string nam = fileName.Substring(0, startIndex);

                string num = j.ToString().PadLeft(digits, '0');

                return nam + num + ext;
            }
            return fileName;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar1.Value = 100;
            button1.Enabled = true;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        #endregion

        #region EXPORT TAB

        string output
        {
            get
            {
                return textBoxOutput.Text;
            }

            set
            {
                textBoxOutput.Text = value;
            }
        }        

        // DESTINATION FILE
        private void buttonBrowseOutputFilename_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxVideoFilename.Text = saveFileDialog1.FileName;
            }
        }

        void start(string commandPath, string commandParams)
        {
            try
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();

                process.StartInfo.UseShellExecute = false;

                process.EnableRaisingEvents = true;

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;

                process.StartInfo.CreateNoWindow = true;

                process.StartInfo.FileName = commandPath;
                process.StartInfo.Arguments = commandParams;

                process.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(DataReceived);
                process.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(ErrorReceived);

                process.Exited += Process_Exited;

                output = string.Format("{0} {1}\n", System.IO.Path.GetFileName(commandPath), commandParams);                

                buttonExport.Enabled = false;
                if (process.Start())
                {
                    backgroundWorker2.RunWorkerAsync(process);
                }
                else
                {
                    buttonExport.Enabled = true;
                }
            }
            catch (System.Exception e)
            {
                buttonExport.Enabled = true;
            }
        }

        private void Process_Exited(object sender, System.EventArgs e)
        {

        }

        void DataReceived(object sender, System.Diagnostics.DataReceivedEventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(eventArgs.Data))
            {
                AddText(eventArgs.Data + "\n");
            }
        }

        void ErrorReceived(object sender, System.Diagnostics.DataReceivedEventArgs eventArgs)
        {
            AddText("\n" + eventArgs.Data + "\n");
        }

        // START
        private void buttonExport_Click(object sender, EventArgs e)
        {
            string appPath = Properties.Settings.Default.app_path;

            string appParams = "";

            if (string.IsNullOrEmpty(textBoxAudioFilename.Text))
            {
                appParams = "-i \"{0}{1}.png\" -pix_fmt yuv420p -nostdin \"{2}\"";
                appParams = appParams.Replace("{0}", System.IO.Path.Combine(textBox2.Text, textBoxFileMask.Text));
                appParams = appParams.Replace("{1}", "%04d");
                appParams = appParams.Replace("{2}", textBoxVideoFilename.Text);
            }
            else
            {
                //, "-filter:v \"crop={0}:{1}:{2}:{3}\""
                appParams = "-nostdin -i \"{0}{1}.png\" -itsoffset {2} -i \"{3}\" -shortest -pix_fmt yuv420p \"{4}\"";
                appParams = appParams.Replace("{0}", System.IO.Path.Combine(textBox2.Text, textBoxFileMask.Text));
                appParams = appParams.Replace("{1}", "%04d");
                appParams = appParams.Replace("{2}", decimal.ToSingle(numericUpDownExportAudioMinute.Value).ToString().Replace(",", "."));
                appParams = appParams.Replace("{3}", textBoxAudioFilename.Text);
                appParams = appParams.Replace("{4}", textBoxVideoFilename.Text);
            }            

            if (System.IO.File.Exists(textBoxVideoFilename.Text))
            {
                if (MessageBox.Show("Delete the file " + textBoxVideoFilename.Text + "?", "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    System.IO.File.Delete(textBoxVideoFilename.Text);
                }
                else
                {
                    return;
                }
            }

            start(appPath, appParams);
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBoxOutput.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBoxOutput.Text = text;
            }
        }
        private void AddText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.textBoxOutput.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(AddText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.textBoxOutput.Text += text;
            }
        }
        delegate void SetEnabledCallback(bool enabled);

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            System.Diagnostics.Process _process = e.Argument as System.Diagnostics.Process;
            {
                _process.BeginErrorReadLine();
                _process.BeginOutputReadLine();
                _process.WaitForExit();
                _process.Close();
                e.Result = _process;
            }
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonExport.Enabled = true;

            System.Diagnostics.Process _process = e.Result as System.Diagnostics.Process;
            if (_process != null)
            {
                MessageBox.Show("File converted!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Error converting file!", "Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        private void buttonBrowseAudio_Click(object sender, EventArgs e)
        {
            if (openFileDialog3.ShowDialog() == DialogResult.OK)
            {
                textBoxAudioFilename.Text = openFileDialog3.FileName;
            }
        }
    }
}
