﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace Base
{
    public class Apha
    {
        public enum AlphaSplitType
        {
            None,
            Black,
            White,
            Unknown
        }

        public static void splitFileColorAndAlpha(string inputFilePath, string outputFilePath, AlphaSplitType type)
        {
            using (Bitmap srcBitmap = new Bitmap(inputFilePath))
            {
                using (Bitmap dstBitmap = new Bitmap(srcBitmap.Width * 2, srcBitmap.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                {
                    #region LOCK SRC

                    // Lock the bitmap's bits.  
                    Rectangle rectSrc = new Rectangle(0, 0, srcBitmap.Width, srcBitmap.Height);
                    System.Drawing.Imaging.BitmapData bmpDataSrc =
                        srcBitmap.LockBits(rectSrc, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                        srcBitmap.PixelFormat);

                    // Get the address of the first line.
                    IntPtr ptrSrc = bmpDataSrc.Scan0;

                    // Declare an array to hold the bytes of the bitmap.
                    int bytesSrc = Math.Abs(bmpDataSrc.Stride) * srcBitmap.Height;
                    byte[] rgbValuesSrc = new byte[bytesSrc];

                    int bytesPerPixelSrc = Bitmap.GetPixelFormatSize(srcBitmap.PixelFormat) / 8;
                    int widthInBytesSrc = bmpDataSrc.Width * bytesPerPixelSrc;

                    // Copy the RGB values into the array.
                    System.Runtime.InteropServices.Marshal.Copy(ptrSrc, rgbValuesSrc, 0, bytesSrc);

                    #endregion

                    #region LOCK DST
                    // Lock the bitmap's bits.  
                    Rectangle rectDst = new Rectangle(0, 0, dstBitmap.Width, dstBitmap.Height);
                    System.Drawing.Imaging.BitmapData bmpDataDst =
                        dstBitmap.LockBits(rectDst, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                        dstBitmap.PixelFormat);

                    // Get the address of the first line.
                    IntPtr ptrDst = bmpDataDst.Scan0;

                    // Declare an array to hold the bytes of the bitmap.
                    int bytesDst = Math.Abs(bmpDataDst.Stride) * dstBitmap.Height;
                    byte[] rgbValuesDst = new byte[bytesDst];

                    int bytesPerPixelDst = Bitmap.GetPixelFormatSize(dstBitmap.PixelFormat) / 8;
                    int widthInBytesDst = bmpDataDst.Width * bytesPerPixelDst;

                    // Copy the RGB values into the array.
                    System.Runtime.InteropServices.Marshal.Copy(ptrDst, rgbValuesDst, 0, bytesDst);

                    #endregion

                    for (int y = 0; y < bmpDataSrc.Height; y++)
                    {
                        int currentLineSrc = y * bmpDataSrc.Stride;
                        int currentLineDst = y * bmpDataDst.Stride;

                        for (int x = 0; x < bmpDataSrc.Width; x++)
                        {
                            byte B = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 0];
                            byte G = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 1];
                            byte R = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 2];
                            byte A = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 3];

                            double dB = B;
                            double dG = G;
                            double dR = R;
                            double dA = A;

                            double dAMax = 255;

                            // STRAIT ALPHA
                            if (type == AlphaSplitType.None)
                            {
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = B;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = G;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = R;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
                            }
                            // BLAK MATTE
                            else if (type == AlphaSplitType.Black)
                            {
                                // Cb = 0
                                // ( Cu ∙ a ) 

                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = (byte)(dB * dA / dAMax);
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = (byte)(dG * dA / dAMax);
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = (byte)(dR * dA / dAMax);
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
                            }
                            // WHITE MATTE
                            else if (type == AlphaSplitType.White)
                            {
                                // Cb = 255
                                // Cm = ( Cu ∙ a ) + [ Cb ∙ ( amax - a ) ]

                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = clamp_0_255((byte)((dB * dA / dAMax) + (dAMax - dA)));
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = clamp_0_255((byte)((dG * dA / dAMax) + (dAMax - dA)));
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = clamp_0_255((byte)((dR * dA / dAMax) + (dAMax - dA)));
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;

                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 0] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 1] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 2] = A;
                                rgbValuesDst[currentLineDst + (x + bmpDataSrc.Width) * bytesPerPixelDst + 3] = 255;
                            }
                        }
                    }

                    // Unlock the bits.
                    srcBitmap.UnlockBits(bmpDataSrc);

                    // Copy the RGB values back to the bitmap
                    System.Runtime.InteropServices.Marshal.Copy(rgbValuesDst, 0, ptrDst, bytesDst);

                    // Unlock the bits.
                    dstBitmap.UnlockBits(bmpDataDst);

                    dstBitmap.Save(outputFilePath);
                }
            }
        }

        public static void splitFileAlpha(string inputFilePath, string outputFilePath, AlphaSplitType type)
        {
            using (Bitmap srcBitmap = new Bitmap(inputFilePath))
            {
                using (Bitmap dstBitmap = new Bitmap(srcBitmap.Width, srcBitmap.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                {
                    #region LOCK SRC

                    // Lock the bitmap's bits.  
                    Rectangle rectSrc = new Rectangle(0, 0, srcBitmap.Width, srcBitmap.Height);
                    System.Drawing.Imaging.BitmapData bmpDataSrc =
                        srcBitmap.LockBits(rectSrc, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                        srcBitmap.PixelFormat);

                    // Get the address of the first line.
                    IntPtr ptrSrc = bmpDataSrc.Scan0;

                    // Declare an array to hold the bytes of the bitmap.
                    int bytesSrc = Math.Abs(bmpDataSrc.Stride) * srcBitmap.Height;
                    byte[] rgbValuesSrc = new byte[bytesSrc];

                    int bytesPerPixelSrc = Bitmap.GetPixelFormatSize(srcBitmap.PixelFormat) / 8;
                    int widthInBytesSrc = bmpDataSrc.Width * bytesPerPixelSrc;

                    // Copy the RGB values into the array.
                    System.Runtime.InteropServices.Marshal.Copy(ptrSrc, rgbValuesSrc, 0, bytesSrc);

                    #endregion

                    #region LOCK DST
                    // Lock the bitmap's bits.  
                    Rectangle rectDst = new Rectangle(0, 0, dstBitmap.Width, dstBitmap.Height);
                    System.Drawing.Imaging.BitmapData bmpDataDst =
                        dstBitmap.LockBits(rectDst, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                        dstBitmap.PixelFormat);

                    // Get the address of the first line.
                    IntPtr ptrDst = bmpDataDst.Scan0;

                    // Declare an array to hold the bytes of the bitmap.
                    int bytesDst = Math.Abs(bmpDataDst.Stride) * dstBitmap.Height;
                    byte[] rgbValuesDst = new byte[bytesDst];

                    int bytesPerPixelDst = Bitmap.GetPixelFormatSize(dstBitmap.PixelFormat) / 8;
                    int widthInBytesDst = bmpDataDst.Width * bytesPerPixelDst;

                    // Copy the RGB values into the array.
                    System.Runtime.InteropServices.Marshal.Copy(ptrDst, rgbValuesDst, 0, bytesDst);

                    #endregion

                    for (int y = 0; y < bmpDataSrc.Height; y++)
                    {
                        int currentLineSrc = y * bmpDataSrc.Stride;
                        int currentLineDst = y * bmpDataDst.Stride;

                        for (int x = 0; x < bmpDataSrc.Width; x++)
                        {
                            byte A = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 3];

                            if (type != AlphaSplitType.None)
                            {
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = A;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = A;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = A;
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;
                            }   
                            else
                            {
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 0] = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 0];
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 1] = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 1];
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 2] = rgbValuesSrc[currentLineSrc + x * bytesPerPixelSrc + 2];
                                rgbValuesDst[currentLineDst + x * bytesPerPixelDst + 3] = 255;
                            }                         
                        }
                    }

                    // Unlock the bits.
                    srcBitmap.UnlockBits(bmpDataSrc);

                    // Copy the RGB values back to the bitmap
                    System.Runtime.InteropServices.Marshal.Copy(rgbValuesDst, 0, ptrDst, bytesDst);

                    // Unlock the bits.
                    dstBitmap.UnlockBits(bmpDataDst);

                    dstBitmap.Save(outputFilePath);
                }
            }
        }

        public static byte clamp_0_255(int b)
        {
            if (b > 255)
                b = 255;
            if (b < 0)
                b = 0;

            return (byte)b;
        }
    }
}
