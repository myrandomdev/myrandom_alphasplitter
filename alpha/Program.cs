﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alpha
{
    class Program
    {
        static void Main(string[] args)
        {
            bool composite = getOutputType(args);

            Base.Apha.AlphaSplitType type = getSplitType(args);
            if (type == Base.Apha.AlphaSplitType.Unknown)
            {
                type = Base.Apha.AlphaSplitType.Black;
            }

            string inputFilePath = getInputFilePath(args);
            if (inputFilePath == null)
            {
                writeHelp();
                return;
            }

            string inputFileName = System.IO.Path.GetFileName(inputFilePath);
            string inputFileDirectory = System.IO.Path.GetDirectoryName(inputFilePath);

            string outputFilePath = getOutputFilePath(args);
            if (outputFilePath == null)
            {
                outputFilePath = System.IO.Path.GetFileNameWithoutExtension(inputFileName) + "_alpha" + "." + System.IO.Path.GetExtension(inputFileName);
            }

            if (System.IO.Path.IsPathRooted(outputFilePath) == false)
            {
                outputFilePath = System.IO.Path.Combine(inputFileDirectory, outputFilePath);
            }

            Console.WriteLine(string.Format("Exporting alpha channel from file '{0}' to '{1}'...", inputFilePath, outputFilePath));

            if (composite)
            {
                Base.Apha.splitFileColorAndAlpha(inputFilePath, outputFilePath, type);
            }
            else
            {
                Base.Apha.splitFileAlpha(inputFilePath, outputFilePath, type);
            }

            Console.WriteLine(string.Format("File '{0}' written.", outputFilePath));
        }

        static void writeHelp()
        {
            Console.WriteLine("alpha -i <input image path> [options]");
            Console.WriteLine("Options:");

            Console.WriteLine("\t -o <output image path>");
            Console.WriteLine("\t  Sets the output image");

            Console.WriteLine("\t -mode:black");
            Console.WriteLine("\t  Will export the image using a black background");

            Console.WriteLine("\t -mode:white");
            Console.WriteLine("\t  Will export the image using a white background");

            Console.WriteLine("\t -mode:none");
            Console.WriteLine("\t  Will only export the colors without transprency");

            Console.WriteLine("\t -type:single");
            Console.WriteLine("\t  Will export only the final map");

            Console.WriteLine("\t -type:composite");
            Console.WriteLine("\t  Will export both the images in a single file. From Left to Right.");
        }

        static string getInputFilePath(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].ToLower() == "-i")
                {
                    if ((i + 1) < args.Length)
                    {                        
                        return args[i + 1];
                    }
                }
            }
            return null;
        }

        static Base.Apha.AlphaSplitType getSplitType(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].ToLower() == "-mode:black")
                {
                    return Base.Apha.AlphaSplitType.Black;
                }
                else if (args[i].ToLower() == "-mode:white")
                {
                    return Base.Apha.AlphaSplitType.White;
                }
                else if (args[i].ToLower() == "-mode:none")
                {
                    return Base.Apha.AlphaSplitType.None;
                }
            }

            return Base.Apha.AlphaSplitType.Unknown;
        }

        static bool getOutputType(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].ToLower() == "-type:single")
                {
                    return false;
                }
                else if (args[i].ToLower() == "-type:composite")
                {
                    return true;
                }                
            }

            return false;
        }

        static string getOutputFilePath(string[] args)
        {
            for (int i = 0; i < args.Length; ++i)
            {
                if (args[i].ToLower() == "-o")
                {
                    if ((i + 1) < args.Length)
                    {
                        return args[i + 1];
                    }
                }
            }
            return null;
        }
    }
}
